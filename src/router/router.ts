import { IRouter } from "../interfaces/IRouter.ts"; // Scheme of the router
import { CrearUsuario } from "../views/main_views.ts";


export const Routes: IRouter[] = [
    {
        RouteName: "CrearUsuario", 
        Pattern: new URLPattern({pathname: "/usuario/crear"}), 
        Methods: ["POST"], 
        View: CrearUsuario
    },
]

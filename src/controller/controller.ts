// Class controller to handle Models
// - Validate model
// - parse model into JSON

import { Base } from "../models/Base.ts";
import { IDate } from "../interfaces/IDate.ts";


export class MainController {
    
    private model: Base;

    constructor(model: Base) {
        this.model = model;
    }

    public validate() {
        const model_object = this.getObject();
        console.log(model_object);
        if (Object.keys(model_object).length <= 1) {
            throw "ModelError";
        }
    }

    private getObject() {
        let res = {};

        for (const [key, value] of Object.entries(this.model)) {
            // validando que ningun atributo del modelo sea indefinido
            // en este caso el modelo no devolvera ningun valor
            if (value === undefined) {
                res = {error: "ModelError"};
                break;
            }
            if (key == "CreationDate" || key == "ModificationDate") {
                const date: IDate = value;
                Object.assign(res, {[key]: date.ParseDate()});
                continue;
            }
            Object.assign(res, {[key]: value})
        }

        return res;
    }


    public getJSON() {
        return JSON.stringify(this.getObject());
    }
}

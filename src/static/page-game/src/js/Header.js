import {
  Button,
  Modal,
  ModalBody
} from "reactstrap";
import "bootstrap/dist/css/bootstrap.css";
import React from "react";

export class Header extends React.Component {
  state = {
    open: false,
    openTwo: false,
  };
  openModal = () => {
    this.setState({ open: !this.state.open });
  };
  openModalTwo = () => {
    this.setState({ openTwo: !this.state.openTwo });
  };
  render() {
    return (
      <>
        <header>
          <h1>Time on Fire</h1>
          <div className="Container-Buttons">
            <Button
              className="btn btn-warning text-danger btn-Modal"
              onClick={this.openModal}
            >
              Log In
            </Button>
            <Button
              className="btn btn-Modal btn-danger text-warning"
              onClick={this.openModalTwo}
            >
              Sign In
            </Button>
          </div>
        </header>

        <Modal isOpen={this.state.open}>
          <ModalBody className="modal-content">
            <form action="" class="formOne" id="formOne" method="get">
              <label>User Name:</label>
              <input
                type="text"
                name="userName"
                id="user"
                class="text-Checkbox text-light bg-dark"
              />
              <br />
              <br />
              <label>Password:</label>
              <input
                type="password"
                name="password"
                class="text-password text-light bg-dark"
                id="pass"
              />
              <br />
              <button
                id="submit"
                class="btn btn-Modal-in bg-warning"
                onclick="validate(SubmitEvent)"
              >
                Get In
              </button>
              <Button onClick={this.openModal} className="btn btn-Modal-in">
                Cerrar
              </Button>
            </form>
          </ModalBody>
        </Modal>

        <Modal isOpen={this.state.openTwo}>
          <ModalBody className="modal-content">
            <form action="" method="get" class="formTwo" id="formTwo">
              <label>Name</label>
              <input
                type="text"
                class="text-Checkbox text-light bg-dark"
                name="name"
                id="name"
              />
              <br />
              <br />
              <label>LastNamame</label>
              <input
                type="text"
                class="text-Checkbox text-light bg-dark"
                name="lastName"
                id="lastName"
              />
              <br />
              <br />
              <label>Age</label>
              <input
                type="text"
                class="text-Checkbox text-light bg-dark"
                name="age"
                id="age"
              />
              <br />
              <br />
              <label>Email</label>
              <input
                type="text"
                class="text-Checkbox text-light bg-dark"
                name="email"
                id="password"
              />
              <br />
              <br />
              <label>Password</label>
              <input
                type="password"
                class="text-password text-light bg-dark"
                name="password"
                id="password"
              />
              <br />
              <br />
              <label>Confirm Password</label>
              <input
                type="password"
                class="text-password text-light bg-dark"
                name="confirmPassword"
                id="confirmPassword"
              />
              <br />
              <br />
              <br />
              <button
                id="submit"
                class="btn btn-Modal-in bg-warning"
                onclick="Registry"
              >
                Guardar
              </button>
              <Button onClick={this.openModalTwo} className="btn btn-Modal-in">
                Cerrar
              </Button>
            </form>
          </ModalBody>
        </Modal>
      </>
    );
  }
}

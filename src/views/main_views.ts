import { MainController } from "../controller/controller.ts";
import { IUser } from "../interfaces/IUser.ts";
import { User } from "../models/User.ts";


export async function CrearUsuario(req: Request): Promise<Response> {
  
    const response_data = {
        data: "",
        status: 200
    };

    await req.text().then((r) => {

        // parsing req into an object
        const parsing: IUser = JSON.parse(r);
        // new model user
        const newUser = new User(parsing);
        // creating new controller
        const controller = new MainController(newUser);
        // validate that the data of the POST request is valid in the model
        controller.validate();
        response_data.data = controller.getJSON();

    })
    .catch((e) => {
        response_data.data = e;
        response_data.status = 500;
    });

    return new Response(response_data.data, {status: response_data.status});
}
